#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFile>
#include <QLabel>
#include <QFontComboBox>
#include <QComboBox>
#include <QPushButton>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

protected:
    virtual void resizeEvent(QResizeEvent *event) override;

private slots:
    void on_saveFileAction_triggered();

    void on_OpenFileAction_triggered();

    void on_underlineAction_triggered(bool checked);

    void on_textEdit_selectionChanged();

    void on_boldAction_triggered(bool checked);

    void on_italicAction_triggered(bool checked);

    void on_centerAction_triggered(bool checked);

    void on_leftAction_triggered(bool checked);

    void on_rightAction_triggered(bool checked);

    void on_justifyAction_triggered(bool checked);

    void on_newFileAction_triggered(bool checked);

private:
    Ui::MainWindow *ui;
    QFile* m_curFile;   // 维护当前编辑文件
    QString m_curDir;   // 维护当前打开目录
    QColor m_curColor;  // 维护当前选择颜色
    /* 主要控件在qtdesigner可视化设置，以下主要为通过代码设置的按钮 */
    /* 控件 */
    QToolBar* m_tb; // 第二行工具栏
    QLabel* m_locShow;  // 界面左下角行列位置
    QLabel* m_charNums; // 界面右下角字数信息
    QFontComboBox* m_fontBox;   // 字体下列框
    QComboBox* m_sizeBox;   // 字号下拉框
    QPushButton* m_setColorBtn; // 设置颜色按钮

    void initUi();  // 主要设置工具栏第二行
    bool createFile();  // 创建文件
    void calRowCol(int& row, int& col); // 计算当前位置行列
};
#endif // MAINWINDOW_H
