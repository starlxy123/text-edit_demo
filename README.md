# 富文本编辑器

## 介绍
#### 开发环境：qt5.15.2 + qmake + gcc
#### 运行环境：win/linux


## 安装教程
#### [点击此处下载源码](https://gitee.com/starlxy123/text-edit_demo)
> 绿色版下载：
#### [点击此处下载windows版本](https://gitee.com/starlxy123/text-edit_demo/blob/release/%E8%BD%AF%E4%BB%B6%E5%8C%85/TextEdit-win-v1.0-release.zip)
#### [点击此处下载linux版本](https://gitee.com/starlxy123/text-edit_demo/blob/release/%E8%BD%AF%E4%BB%B6%E5%8C%85/TextEdit-linux-v1.0-release.tar.xz)


## 使用说明
### 当前功能
1. 新建/打开/保存文件
2. 设置文字加粗、下划线、斜体
3. 文字对齐方式：左右对齐、居中、两端对齐
3. 字号、字体、颜色选择

## 更多
更多功能持续更新...


